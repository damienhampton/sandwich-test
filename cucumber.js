// cucumber.js
let common = [
  "test/acceptance/**/*.feature", // Specify our feature files
  "--require-module ts-node/register", // Load TypeScript module
  "--require-module tsconfig-paths/register",
  "--require test/acceptance/step-definitions/**/*.ts", // Load step definitions
  "--format progress-bar", // Load custom formatter
  "--format @cucumber/pretty-formatter", // Load custom formatter
  "--publish-quiet",
].join(" ");

module.exports = {
  default: common,
};
