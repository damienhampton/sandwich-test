# Sandwich Order Service

## Install

`npm i`

## Test

`npm test` (unit tests)

`npm run test:acceptance` (acceptance tests)

`npm run test:all` (all tests)

## Run

Either:
`npm run start:dev`

Or:
`docker compose up`

Use Postman/Insomnia to make HTTP requests, e.g.

### Create order

```
POST /order HTTP/1.1
Host: localhost:3000
Content-Type: application/json
Content-Length: 29

{
"recipient": "Damien"
}
```

### List tasks

```
GET /tasks HTTP/1.1
Host: localhost:3000
```


## Assumptions

- Building a REST service to show a few tricks.
- Should not store time as state. Service will assume tasks start from 0 minutes and 0 seconds (I started with the opposite assumption, but it didn't seem to make as much sense)
- Task ids should not be stateful, but they should be in sequence (that is to say, the first task will always be task #1)

## Notes

Things I've tried to demonstrate:

- Use of TDD using Mocha and Chai (`test/unit/SandwichOrderHelper.spec.ts`, `test/unit/SandwichOrderService.spec.ts`)
- Use of ATDD using Cucumber (`test/acceptance/sandwitch-order.feature`)
- Dependency injection (`src/create-app.ts`)
- Separation of wiring (`src/create-app.ts`) from business logic (`src/services/SandWichOrderService.ts`)
- Use of pure functions (`src/helpers/SandwichOrderHelper.ts`)
- CI configured (`.gitlab-ci.yml`)
- Minimal reliance of 3rd party frameworks (though I think frameworks like Nestjs can be very valuable!)

Additionally, I have eslint and prettier installed and have them configured in Intellij IDEA to get automatic linting and formatting.

There is no error checking and only a in-memory db is used.