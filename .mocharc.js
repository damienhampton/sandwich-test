module.exports = {
  require: ["ts-node/register", "tsconfig-paths/register"],
  spec: ["test/unit/**/*.spec.ts"],
  "watch-files": ["src/**/*.ts", "test/unit/**/*.ts"],
};
