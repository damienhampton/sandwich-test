import dotenv from "dotenv";
dotenv.config();

import { createApp } from "./create-app";
import { SandwichOrderInMemoryRepository } from "./repositories/SandwichOrderRepository";

const port = process.env.PORT || 3000;

const inMemoryRepo = new SandwichOrderInMemoryRepository();
const app = createApp(inMemoryRepo);

app.listen(port);
