import express from "express";
import { SandwichOrderService } from "./services/SandwichOrderService";
import { SandwichOrderRepositoryInterface } from "./repositories/SandwichOrderRepository";
import bodyParser from "body-parser";
import { SandwichOrderController } from "./controllers/SandwichOrderController";
import { SandwichOrderRoutes } from "./routes/SandwichOrderRoutes";

export function createApp(
  sandwichOrderRepository: SandwichOrderRepositoryInterface
) {
  const sandwichOrderService = new SandwichOrderService(
    sandwichOrderRepository
  );
  const sandwichOrderController = new SandwichOrderController(
    sandwichOrderService
  );
  const sandwichOrderRouter = new SandwichOrderRoutes(sandwichOrderController);

  const app = express();
  app.use(bodyParser.urlencoded({ extended: true }));
  app.use(bodyParser.json());

  app.use(sandwichOrderRouter.getRouter());

  return app;
}
