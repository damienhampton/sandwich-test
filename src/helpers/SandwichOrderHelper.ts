import moment from "moment/moment";
import { BaseTask, BreakTask, Order, Task, TaskType } from "../models/Sandwich";
import { Time } from "../models/Time";

export const SANDWICH_MAKE_DURATION = 2.5; //minutes
export const SANDWICH_SERVE_DURATION = 1; //minutes
export const SANDWICH_TOTAL_DURATION =
  SANDWICH_MAKE_DURATION + SANDWICH_SERVE_DURATION; //minutes

export class SandwichOrderHelper {
  public static getTasks(sandwichOrders: Order[]): BaseTask[] {
    const taskNumberIncrementor = numberIncrememtor();
    const time = moment();
    time.set("minutes", 0);
    time.set("seconds", 0);
    return [
      ...sandwichOrders.flatMap((sandwichOrder, ii) => {
        const startTime = moment(time).add(
          ii * SANDWICH_TOTAL_DURATION,
          "minutes"
        );

        return [
          new Task(
            taskNumberIncrementor(),
            sandwichOrder.recipient,
            sandwichOrder.sandwichType,
            sandwichOrder.sequenceNumber,
            Time.fromDate(startTime.toDate()),
            TaskType.Make
          ),
          new Task(
            taskNumberIncrementor(),
            sandwichOrder.recipient,
            sandwichOrder.sandwichType,
            sandwichOrder.sequenceNumber,
            Time.fromDate(
              moment(startTime).add(SANDWICH_MAKE_DURATION, "minutes").toDate()
            ),
            TaskType.Serve
          ),
        ];
      }),
      new BreakTask(
        taskNumberIncrementor(),
        Time.fromDate(
          moment(time)
            .add(sandwichOrders.length * SANDWICH_TOTAL_DURATION, "minutes")
            .toDate()
        )
      ),
    ];
  }
}

export const numberIncrememtor =
  (start = 1) =>
  () =>
    start++;
