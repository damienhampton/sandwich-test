import { Request, Response } from "express";
import { SandwichError } from "../models/errors";

export const createRoute =
  <R>(controller: (req: Request) => Promise<R>) =>
  async (req: Request, res: Response) => {
    try {
      const result = await controller(req);
      res.json(result);
    } catch (_e) {
      const e = _e as Error;
      if (e instanceof SandwichError) {
        res.status(e.statusCode).json({ error: e.message });
        return;
      }
      res.status(500).json({ error: e.message });
    }
  };
