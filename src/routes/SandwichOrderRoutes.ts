import express from "express";
import { SandwichOrderController } from "../controllers/SandwichOrderController";
import { createRoute } from "../helpers/router-helper";

export class SandwichOrderRoutes {
  constructor(private controller: SandwichOrderController) {}
  getRouter() {
    const router = express.Router();
    router.post(
      "/order",
      createRoute((req) => this.controller.createOrder(req))
    );

    router.get(
      "/tasks",
      createRoute(() => this.controller.getTasks())
    );

    return router;
  }
}
