import { SandwichOrderService } from "src/services/SandwichOrderService";
import { Request } from "express";
import { BadParametersError } from "../models/errors";

export class SandwichOrderController {
  constructor(private sandwichOrderService: SandwichOrderService) {}

  public async createOrder(req: Request) {
    this.validateBody(req);
    return { order: await this.sandwichOrderService.createOrder(req.body) };
  }

  public async getTasks() {
    return {
      tasks: (await this.sandwichOrderService.getTasks()).map((task) =>
        task.format()
      ),
    };
  }

  private validateBody(req: Request) {
    if (!req.body.recipient) {
      throw new BadParametersError("missing recipient");
    }
    if (!req.body.sandwichType) {
      throw new BadParametersError("missing sandwichType");
    }
  }
}
