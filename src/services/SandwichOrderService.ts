import { BaseTask, Order } from "../models/Sandwich";
import { SandwichOrderHelper } from "../helpers/SandwichOrderHelper";
import { SandwichOrderRepositoryInterface } from "../repositories/SandwichOrderRepository";

export class SandwichOrderService {
  constructor(private readonly repo: SandwichOrderRepositoryInterface) {}

  public async createOrder(
    partialOrder: Omit<Order, "sequenceNumber">
  ): Promise<Order> {
    const nextId = await this.repo.getNextId();
    const order = new Order(
      partialOrder.recipient,
      partialOrder.sandwichType,
      nextId
    );
    await this.repo.addOrder(order);
    return order;
  }

  public async getTasks(): Promise<BaseTask[]> {
    const orders = await this.repo.getOrders();
    return SandwichOrderHelper.getTasks(orders);
  }
}
