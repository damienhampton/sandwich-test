export abstract class SandwichError extends Error {
  abstract statusCode: number;
}
export class BadParametersError extends SandwichError {
  public statusCode = 400;
}
