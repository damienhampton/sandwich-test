export class Time {
  constructor(public min: number, public sec: number) {}
  toString() {
    return `${padLeft(this.min)}:${padLeft(this.sec)}`;
  }
  static fromDate(date: Date) {
    return new Time(date.getMinutes(), date.getSeconds());
  }
}

function padLeft(num: number, len = 2) {
  let str = num.toString();
  while (str.length < len) {
    str = "0" + str;
  }
  return str;
}
