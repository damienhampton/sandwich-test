import { Time } from "./Time";

export enum TaskType {
  Make = "Make",
  Serve = "Serve",
}

export class Order {
  constructor(
    public recipient: string,
    public sandwichType: string,
    public sequenceNumber: number
  ) {}
}

export abstract class BaseTask {
  abstract format(): TaskResponse;
}

export class Task extends BaseTask {
  constructor(
    public taskNumber: number,
    public recipient: string,
    public sandwichType: string,
    public sequenceNumber: number,
    public time: Time,
    public task: TaskType
  ) {
    super();
  }

  public override format(): TaskResponse {
    return {
      taskNumber: this.taskNumber,
      time: this.time.toString(),
      description: `${this.task} ${this.sandwichType} sandwich #${this.sequenceNumber} for ${this.recipient}`,
    };
  }
}

export class BreakTask extends BaseTask {
  constructor(public taskNumber: number, public time: Time) {
    super();
  }

  public override format(): TaskResponse {
    return {
      taskNumber: this.taskNumber,
      time: this.time.toString(),
      description: "Take a break.",
    };
  }
}

export type TaskResponse = {
  taskNumber: number;
  time: string;
  description: string;
};
