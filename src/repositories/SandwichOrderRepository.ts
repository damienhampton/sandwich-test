import { Order } from "../models/Sandwich";

export interface SandwichOrderRepositoryInterface {
  addOrder(order: Order): Promise<void>;
  getOrders(): Promise<Order[]>;

  getNextId(): Promise<number>;
}

export class SandwichOrderInMemoryRepository
  implements SandwichOrderRepositoryInterface
{
  private orders: Order[] = [];
  private nextId = 1;
  async addOrder(order: Order) {
    this.orders.push(order);
  }
  async getOrders(): Promise<Order[]> {
    return this.orders;
  }
  async getNextId(): Promise<number> {
    return this.nextId++;
  }
}
