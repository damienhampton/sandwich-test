import { createApp } from "../../../src/create-app";
import { SandwichOrderInMemoryRepository } from "../../../src/repositories/SandwichOrderRepository";

export function createTestApp() {
  const repo = new SandwichOrderInMemoryRepository();
  return createApp(repo);
}
