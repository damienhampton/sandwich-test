import { World } from "@cucumber/cucumber";
import { Task } from "../../../src/models/Sandwich";
import moment, { Moment } from "moment/moment";

export type TableDefinition<T> = {
  hashes(): T[];
};

export type TaskTestData = {
  taskNumber: string;
  time: string;
  description: string;
};

export class CustomWorld extends World {
  public tasks: Task[] = [];
  public orderResponse: any;
  public time: Moment = moment();
}
