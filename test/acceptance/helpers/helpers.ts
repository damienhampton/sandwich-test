import { TaskTestData } from "./types";
import { TaskResponse } from "src/models/Sandwich";

export function convertToTaskResponse({
  taskNumber,
  time,
  description,
}: TaskTestData): TaskResponse {
  return {
    taskNumber: parseInt(taskNumber),
    time,
    description,
  };
}
