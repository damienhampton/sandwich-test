Feature: SandwichOrderFeature
  Each handmade sandwich takes me 2½ minutes to make.
  It then takes me 1 minute to serve the sandwich and take payment.
  I take a break when there aren’t any orders to make or serve.
  The schedule must contain the sequence number, time, task, and recipient.

  Scenario: Preparing 3 sandwiches
    Given there are sandwich orders to prepare
      | orderNumber | recipient  | sandwichType |
      | 1           | Stavros    | ham |
      | 2           | Anisa      | cheese |
      | 3           | Adeel      | tomato |
    When I get the task list
    Then the task list should be
      | taskNumber | time   | description                    |
      | 1          | 00:00  | Make ham sandwich #1 for Stavros   |
      | 2          | 02:30  | Serve ham sandwich #1 for Stavros  |
      | 3          | 03:30  | Make cheese sandwich #2 for Anisa     |
      | 4          | 06:00  | Serve cheese sandwich #2 for Anisa    |
      | 5          | 07:00  | Make tomato sandwich #3 for Adeel     |
      | 6          | 09:30  | Serve tomato sandwich #3 for Adeel    |
      | 7          | 10:30  | Take a break.                  |

  Scenario: Add valid order
    When I add a valid order
    Then the response should be successful

  Scenario: Add invalid order
    When I add a invalid order
    Then the response should be unsuccessful

