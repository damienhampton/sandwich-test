import { expect } from "chai";
import { Given, Then, When } from "@cucumber/cucumber";
import request from "supertest";
import { Order } from "src/models/Sandwich";
import { CustomWorld, TableDefinition, TaskTestData } from "../helpers/types";
import { convertToTaskResponse } from "../helpers/helpers";
import { createTestApp } from "../helpers/create-test-app";

const app = createTestApp();

Given(
  "there are sandwich orders to prepare",
  { timeout: 60 * 1000 },
  async function (this: CustomWorld, dataTable: TableDefinition<Order>) {
    const data = dataTable.hashes();

    for (let ii = 0; ii < data.length; ii++) {
      await request(app).post("/order").send({
        recipient: data[ii].recipient,
        sandwichType: data[ii].sandwichType,
      });
    }
  }
);

When("I get the task list", async function (this: CustomWorld) {
  this.tasks = (await request(app).get("/tasks")).body.tasks;
});

Then(
  "the task list should be",
  async function (this: CustomWorld, dataTable: TableDefinition<TaskTestData>) {
    await Promise.all(
      dataTable.hashes().map((testTask) => {
        const expectedTaskResponse = convertToTaskResponse(testTask);
        const task = this.tasks.find(
          (task) => task.taskNumber === expectedTaskResponse.taskNumber
        );
        expect(task).to.exist;
        expect(task).to.deep.equal(expectedTaskResponse);
      })
    );
  }
);

When("I add a valid order", async function (this: CustomWorld) {
  this.orderResponse = await request(app)
    .post("/order")
    .send({ recipient: "Damien", sandwichType: "ham" });
});

Then("the response should be successful", function (this: CustomWorld) {
  expect(this.orderResponse.res.statusCode).to.deep.equal(200);
});

When("I add a invalid order", async function (this: CustomWorld) {
  this.orderResponse = await request(app).post("/order").send({});
});

Then("the response should be unsuccessful", function (this: CustomWorld) {
  expect(this.orderResponse.res.statusCode).to.deep.equal(400);
});
