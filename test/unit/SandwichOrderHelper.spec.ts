import { expect } from "chai";
import moment from "moment";
import { BreakTask, Order, Task, TaskType } from "src/models/Sandwich";
import {
  numberIncrememtor,
  SANDWICH_MAKE_DURATION,
  SANDWICH_SERVE_DURATION,
  SandwichOrderHelper,
} from "src/helpers/SandwichOrderHelper";
import { Time } from "src/models/Time";

describe("Sandwich Order Helper", () => {
  describe("Creating tasks for one sandwich", () => {
    it("should return two tasks - make and serve", () => {
      const taskNumberIncrementor = numberIncrememtor();
      const now = moment("2022-12-23T00:00:00Z");

      const expectedTasks = [
        new Task(
          taskNumberIncrementor(),
          "Damien",
          "ham",
          1,
          Time.fromDate(now.toDate()),
          TaskType.Make
        ),
        new Task(
          taskNumberIncrementor(),
          "Damien",
          "ham",
          1,
          Time.fromDate(now.add(SANDWICH_MAKE_DURATION, "minutes").toDate()),
          TaskType.Serve
        ),
        new Task(
          taskNumberIncrementor(),
          "Juliet",
          "cheese",
          2,
          Time.fromDate(now.add(SANDWICH_SERVE_DURATION, "minutes").toDate()),
          TaskType.Make
        ),
        new Task(
          taskNumberIncrementor(),
          "Juliet",
          "cheese",
          2,
          Time.fromDate(now.add(SANDWICH_MAKE_DURATION, "minutes").toDate()),
          TaskType.Serve
        ),
        new BreakTask(
          taskNumberIncrementor(),
          Time.fromDate(now.add(SANDWICH_SERVE_DURATION, "minutes").toDate())
        ),
      ];

      const sandwichOrders = [
        new Order("Damien", "ham", 1),
        new Order("Juliet", "cheese", 2),
      ];

      const tasks = SandwichOrderHelper.getTasks(sandwichOrders);

      expect(tasks).to.deep.equal(expectedTasks);
    });
  });
});
