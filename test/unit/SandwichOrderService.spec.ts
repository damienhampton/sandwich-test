import sinon from "sinon";
import { expect } from "chai";
import { Time } from "src/models/Time";
import { BreakTask, Order, Task, TaskType } from "src/models/Sandwich";
import { SandwichOrderInMemoryRepository } from "src/repositories/SandwichOrderRepository";
import { SandwichOrderService } from "src/services/SandwichOrderService";

describe("SandwichOrderService", () => {
  describe("createOrder", () => {
    it("should create a new order", async () => {
      const orderRepository = new SandwichOrderInMemoryRepository();
      const addOrderSpy = sinon.spy(orderRepository, "addOrder");
      sinon.stub(orderRepository, "getNextId").resolves(1);

      const service = new SandwichOrderService(orderRepository);
      await service.createOrder({ recipient: "Damien", sandwichType: "ham" });

      sinon.assert.calledOnceWithExactly(
        addOrderSpy,
        new Order("Damien", "ham", 1)
      );
    });
  });

  describe("getTasks", () => {
    it("should return two tasks", async () => {
      const orderRepository = new SandwichOrderInMemoryRepository();
      sinon
        .stub(orderRepository, "getOrders")
        .resolves([new Order("Damien", "ham", 1)]);

      const service = new SandwichOrderService(orderRepository);
      expect(await service.getTasks()).to.deep.equal([
        new Task(1, "Damien", "ham", 1, new Time(0, 0), TaskType.Make),
        new Task(2, "Damien", "ham", 1, new Time(2, 30), TaskType.Serve),
        new BreakTask(3, new Time(3, 30)),
      ]);
    });
  });
});
